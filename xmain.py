from xfunc import *

def main():
    cfg = load_config("config.ini", "xdefault")
    nerr = check_file(cfg["F_USE"]) + check_file(cfg["F_GLOVE"])
    if nerr > 0: return nerr

    # Load and encode data.
    blog_list = load_blog_list(cfg["F_USE"])
    post_train, author_train, post_test, author_test = load_corpus_subset(blog_list, int(cfg["TRAIN_NPOST"]))

    tkr = make_tokenizer(post_train)
    vocab_nword = len(tkr.word_index) + 1
    emat = make_embedding_matrix(cfg["F_GLOVE"], tkr.word_index, vocab_nword, int(cfg["GLOVE_NWORD"]))

    post_maxword, seq_train, seq_test = make_seq(post_train, post_test, tkr)
    cat_train = kunp.to_categorical(author_train, num_classes=None)
    cat_test = kunp.to_categorical(author_test, num_classes=None)
    seq_train, cat_train = shuffle_training_data(seq_train, cat_train)

    # Define model.
    ncand = len(blog_list)
    model = define_cnn(tkr.word_index, emat, GLOVE_NWORD, post_maxword, ncand)

    # Fit model.
    model.fit([seq_train, seq_train], cat_train, epochs=15, batch_size=50)
    model.compile(loss="categorical_crossentropy", optimizer="rmsprop",
                  metrics=["acc"])
    loss, acc = model.evaluate([seq_test, seq_test], cat_test, verbose=0)
    print("Test accuracy: %f" % (acc * 100))

    return 0

if __name__ == "__main__":
    main()
