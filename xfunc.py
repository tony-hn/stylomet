import configparser as cp
import keras.preprocessing.text as kpt
import keras.preprocessing.sequence as kps
import keras.utils.np_utils as kunp
import keras.layers as kl
import keras.models as km
import numpy as np
import os

import xparse

def load_config(file, section):
    parser = cp.ConfigParser()
    parser.read(file)
    return parser[section]

def check_file(file):
    if not os.path.isfile(file):
        print("Not a file. %s" % file)
        return 1
    return 0

# Take a file containing a list of blog paths,
# one (absolute or relative) path per line.
# Return the list of blog paths.
def load_blog_list(fl_blog):
    with open(fl_blog) as f0:
        l_blog = f0.readlines()
    return [x.strip() for x in l_blog]

# Take an (absolute or relative) path to a directory containing blog files.
# The directory should be as downloaded and unzipped by the script provided.
def load_corpus_subset(l_blog, train_npost):
    post_train = []
    author_train = []
    post_test = []
    author_test = []

    for i0 in range(0, len(l_blog)):
        parser = xparse.html_blog_to_post_list()

        with open(l_blog[i0], encoding="latin1") as f0:
            parser.feed(f0.read())

        post_train.extend(parser.post[0:train_npost].copy())
        author_train.extend([i0] * train_npost)

        post_test.extend(parser.post[train_npost:(train_npost+40)].copy())
        author_test.extend([i0] * (len(parser.post) - train_npost))

    return post_train, author_train, post_test, author_test

def make_tokenizer(data):
    tkr = kpt.Tokenizer()
    tkr.fit_on_texts(data)
    return tkr

# Take the list of training posts, the list of test posts, and the tokenizer.
# Return the maximum number of words in any training post,
# the training posts as padded word sequences, and
# the test posts as padded word sequences.
def make_seq(post_train, post_test, tkr):
    # Assume one space between words, as we imposed in the parser.
    post_maxword = max([x.count(" ") for x in post_train]) + 1

    x0_train = tkr.texts_to_sequences(post_train)
    x0_test = tkr.texts_to_sequences(post_test)
    
    x1_train = kps.pad_sequences(x0_train, maxlen=post_maxword)
    x1_test = kps.pad_sequences(x0_test, maxlen=post_maxword)

    return post_maxword, x1_train, x1_test

# Take an (absolute or relative) path to a file containing word vectors.
# This file should have GLOVE_NWORD + 1 columns.
# The first column should be strings that do not contain whitespace.
# The remaining columns should be floats.
# Return the word vectors as a dictionary with string keys and np.array
# values.
def load_glove(file_glove, glove_nword):
    dict_glove = {}
    wrong_length = {}
    failed_line = []

    with open(file_glove) as f0:
        for line_string in f0:
            try:
                line_list = line_string.split()
                word = line_list[0]
                coef = np.asarray(line_list[1:], dtype = 'float32')
                if len(coef) == glove_nword:
                    dict_glove[word] = coef
                else:
                    wrong_length[word] = line_string
            except:
                failed_line.append(line_string)

    if len(wrong_length) > 0 or len(failed_line) > 0:
        print("In parsing of glove file.")
        print("  Number of lines of wrong length = %d" % len(wrong_length))
        print("  Number of lines not parsable to floats = %d" % len(failed_line))
        #sys.exit(len(wrong_length) + len(failed_line))

    return dict_glove

# Make embedding matrix from glove file.
# file_glove is a path to the glove file.
# dict_tkr is a tokenizer.word_index, which is a dictionary-like object.
# vocab_nword is an integer representing the number of distinct words in posts corpus subset we are using.
# In crapcode, dict_glove = embedding_index, dict_tkr = word_index.
def make_embedding_matrix(file_glove, dict_tkr, vocab_nword, glove_nword):
    dict_glove = load_glove(file_glove, glove_nword)
    emat = np.zeros((vocab_nword, glove_nword))
    for word, i in dict_tkr.items():
        evec = dict_glove.get(word)
        if evec is not None:
            # Words not found in dict_glove will be all-zeros.
            emat[i] = evec
    return emat

def shuffle_training_data(seq_train, cat_train):
    ix = np.arange(seq_train.shape[0])
    np.random.shuffle(ix)
    seq_train = seq_train[ix]
    cat_train = cat_train[ix]
    return seq_train, cat_train

# Take the word_index from a keras tokenizer, trained on the training data,
# an embedding matrix built for each word in the training data,
# the number of columns in the embedding matrix,
# the maximum number of words in a single blog post, and
# the number of candidate authors.
# Compile the "word word" model of Sebastian Ruder et al. (2016).
# Return the CNN model object.
def define_cnn(word_index, emat, emat_ncol, input_maxword, ncand):
    embed1 = km.Sequential()
    embed1.add(kl.Embedding(len(word_index) + 1, emat_ncol,
        weights=[emat], input_length=input_maxword, trainable=False))
    embed2 = km.Sequential()
    embed2.add(kl.Embedding(len(word_index) + 1, emat_ncol,
        weights=[emat], input_length=input_maxword, trainable=True))
    catted = kl.concatenate([embed1.output, embed2.output], axis=-1)

    model3 = km.Sequential()
    model3.add(kl.convolutional.Conv1D(64, 5, activation="relu"))
    model3.add(kl.convolutional.MaxPooling1D(input_maxword - 5 + 1))
    model3.add(kl.Flatten())
    model3.add(kl.Dense(256, activation="relu"))
    model3.add(kl.Dropout(0.5))
    model3.add(kl.Dense(ncand, activation="softmax"))
    model3_output = model3(catted)

    model = km.Model([embed1.input, embed2.input], model3_output)
    model.compile(loss='categorical_crossentropy', optimizer='rmsprop',
                  metrics=['acc'])

    print(model.summary())
    return model
