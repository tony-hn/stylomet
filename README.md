Stylometry demo
====
Identify the author of a piece of writing from among a set of candidate authors,
using the convolutional neural network of Sebastian Ruder et al. (2016).

Requirement
----
- Python 3.8
- Keras 2.2.4
- numpy
- `zdownload.sh` requires `wget`
- `zscan.sh` requires `POSIX.1`

Any version of Python with major number 3 should work.
Any version of Keras >=2 might work, but we have tested only v2.2.4.
The Keras team frequently make breaking changes to their API.

How to use
----
````
./zdownload.sh
python3 xmain.py
````
The first line downloads the required data files to the script's working
directory. If you do not have `wget`, you can do this by hand.

Both `config.ini` and `use_blogs.txt` assume the data are downloaded to the
same directory that holds `xmain.py`. That is not the best place to store data.
If you put the data somewhere else, update `config.ini` and `use_blogs.txt`
as necessary before proceeding to run the second line above.

The second line does the data preprocessing and modelling.

If you want to try running with more than 100 candidate authors, take a look
at `zscan.sh` and modify as necessary.

Data
----
- The corpus of input text is from J. Schler et al. (2006).
  [html](https://u.cs.biu.ac.il/~koppel/BlogCorpus.htm)
- Each blog (i.e. set of posts by an author) is stored in a separate file.
- Each file name has the form blogger_id.sex.age.industry.astrological_sign.
- Each file contains all posts for the given blogger_id.
- Each post contains >=200 occurances of "common English words"
  and has had almost all formatting stripped.

- Word vectors are from Jeffrey Pennington et al. (2014).
  [html](https://nlp.stanford.edu/projects/glove/)

Implementation note
----
Preprocessing
- The authors with the greatest number of posts are selected.
- 80 posts per author are used for training.
  40 further posts per author are used for testing.

Model
- The model is due to Sebastian Ruder et al. (2016).
  Specifically, the "word word" variant.
  [html](https://arxiv.org/abs/1609.06686)
- Code inspiration is from
  [here](https://github.com/asad1996172/Authorship-attribution-using-CNN).

Reference
----
- Jeffrey Pennington, Richard Socher, and Christopher D. Manning (2014). GloVe: Global Vectors for Word Representation.
- J. Schler, M. Koppel, S. Argamon and J. Pennebaker (2006). Effects of Age and Gender on Blogging in Proceedings of 2006 AAAI Spring Symposium on Computational Approaches for Analyzing Weblogs.
- Sebastian Ruder, Parsa Ghaffari, John G. Breslin (2016). Character-level and Multi-channel Convolutional Neural Networks for Large-scale Authorship Attribution.
