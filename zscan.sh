grep -c '/post' blogs/* |
  awk 'BEGIN{FS=":"} {printf "%8s\t%s\n", $2, $1}' |
  sort -rn > npost.txt

head -n100 npost.txt |
  awk '{print $2}' > use_blogs.txt
