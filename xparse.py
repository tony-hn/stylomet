import html.parser as hp
import re

class html_blog_to_post_list(hp.HTMLParser):
    RE_WS = re.compile(r"\s\s+")
    TBL = "".maketrans("", "", "0123456789!#$%&()*+,-./:;<=>?@[\\]^_{|}~")

    def __init__(self):
        super().__init__()
        self.post = []

    def handle_data(self, data):
        if self.get_starttag_text() == "<post>":
            x0 = data.strip()
            # Keras text_to_word_sequence uses too much memory. Do it ourselves.
            if len(x0) > 0:
                # Replace consecutive whitespace with a single space.
                x1 = self.RE_WS.sub(" ", x0)
                # Delete the characters specified in TBL.
                x2 = x1.translate(self.TBL)
                self.post.append(x2)
